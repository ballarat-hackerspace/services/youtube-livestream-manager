# Live YouTube Stream Simple Wrapper for ffmpeg

Turns out it's difficult to keep a 24x7 webcam live stream to YouTube running
because ffmpeg has so many failure modes this is our solution to try and keep
it up and running.

## Requirements

  1. ffmpeg
  1. a music file in aac 44.1kHz 128bit format
  1. a web/security cam with an rtsp streaming url
  1. your YouTube stream key

## Install

    git clone https://gitlab.com/ballarat-hackerspace/services/youtube-livestream-manager.git /opt/live_stream
    cd /opt/live_stream
    cp config.ini.dist config.ini
    vi config.ini
      [ fill in config.ini with your values ]
    pip install -r requirements.txt
    sudo loginctl enable-linger $(whoami)
    systemctl --user enable $(pwd)/youtube_stream.service
    systemctl --user enable $(pwd)/youtube_stream_backup.service
    systemctl --user start youtube_stream.service
    systemctl --user start youtube_stream_backup.service

### Display notes to viewers

There is also an example script for rotating through a bunch of notes to
display on the live stream but it's left to you to decide how best you want to
deal with that.

## Controlling it

Use the usual systemd commands, e.g.:

    systemctl --user status youtube_stream.service
    systemctl --user stop youtube_stream.service
    systemctl --user start youtube_stream.service
    systemctl --user restart youtube_stream.service

## Warning

Your YouTube stream key will be visible in the process list because of how
ffmpeg works, you may not want to run this on a machine where people you don't
trust have any type of access (e.g. desktop, ssh, etc.)
